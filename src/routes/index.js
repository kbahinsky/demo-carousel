import AppRoot from '../components/AppRoot.vue'

import Home from '../pages/Home.vue'
import Contacts from '../pages/Contacts.vue'
import About from '../pages/About.vue'

export default [
	{ 
		path: '/', 
		component: AppRoot,

		children: [
			{
				path: '/', 
				name: 'home', 
				component: Home
			},
			{
				path: '/about',
				name: 'about',
				component: About,
			},
			{
				path: '/contacts',
				name: 'contacts',
				component: Contacts,
			}
		]
	}
]